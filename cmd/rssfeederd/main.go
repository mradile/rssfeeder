package main

import (
	"os"

	"gitlab.com/mradile/rssfeeder/server/http"

	homedir "github.com/mitchellh/go-homedir"
	"github.com/urfave/cli"

	log "github.com/sirupsen/logrus"

	"gitlab.com/mradile/rssfeeder/server/feeddelivering"
	"gitlab.com/mradile/rssfeeder/server/feeding"
	"gitlab.com/mradile/rssfeeder/server/feedlisting"
	"gitlab.com/mradile/rssfeeder/server/storage"
)

var version string
var verbose bool
var debug bool

var port int
var dbPath string
var serverURI string
var secret string
var login string
var password string

func main() {
	cliApp := cli.NewApp()
	cliApp.Name = "rssfeederd"
	cliApp.Usage = "rssfeederd run"
	cliApp.Version = version

	cliApp.Flags = []cli.Flag{
		cli.BoolFlag{
			Name:        "debug",
			Usage:       "prints trace level log messages",
			EnvVar:      "DEBUG",
			Destination: &debug,
		},
		cli.BoolFlag{
			Name:        "verbose",
			Usage:       "prints debug level log messages",
			EnvVar:      "VERBOSE",
			Destination: &verbose,
		},
	}

	cliApp.Commands = []cli.Command{
		{
			Name:  "run",
			Usage: "start the service",
			Action: func(c *cli.Context) error {
				setLogOptions()

				initConfig()

				stor := storage.NewStorage(dbPath)
				defer stor.Close()

				feeder := feeding.Service(stor)
				feedLister := feedlisting.NewService(stor, serverURI)
				feedDeliverer := feeddelivering.NewService(stor, serverURI)

				if secret == "" {
					log.Fatal("no secret provided")
				}
				config := &http.Config{
					ServerURI: serverURI,
					Secret:    secret,
					Login:     login,
					Password:  password,
				}
				handler := http.NewHandler(config, feeder, feedLister, feedDeliverer)
				server := http.NewServer(port, handler)

				return server.Start()
			},
			Flags: []cli.Flag{
				cli.IntFlag{
					Name:        "port, p",
					Value:       8000,
					Usage:       "`port` for service to listen on",
					EnvVar:      "PORT",
					Destination: &port,
				},
				cli.StringFlag{
					Name:        "db-path",
					Value:       GetDBDefaultPath(),
					Usage:       "`path` for to store database in",
					EnvVar:      "DB_PATH",
					Destination: &dbPath,
				},
				cli.StringFlag{
					Name:        "uri",
					Value:       "http://localhost:8000",
					Usage:       "`uri` where feeds will be accessible",
					EnvVar:      "SERVER_URI",
					Destination: &serverURI,
				},
				cli.StringFlag{
					Name:        "secret",
					Usage:       "`secret` for generatring jwt tokens",
					EnvVar:      "SECRET",
					Destination: &secret,
				},
				cli.StringFlag{
					Name:        "login",
					Value:       "admin",
					Usage:       "`login` for accessing the service",
					EnvVar:      "LOGIN",
					Destination: &login,
				},
				cli.StringFlag{
					Name:        "password",
					Value:       "admin",
					Usage:       "`password` for accessing the service",
					EnvVar:      "PASSWORD",
					Destination: &password,
				},
			},
		},
	}

	err := cliApp.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}

func setLogOptions() {
	if debug {
		log.SetLevel(log.TraceLevel)
	} else if verbose {
		log.SetLevel(log.DebugLevel)
	} else {
		log.SetLevel(log.InfoLevel)
	}
}

func initConfig() error {

	return nil
}

//GetDBDefaultPath returns the users $HOME as path for storing the db file
func GetDBDefaultPath() string {
	home, err := homedir.Dir()
	if err != nil {
		log.Fatalf("Could not determine database default path: %v", err)
	}
	return home
}
