package main

import (
	"fmt"
	"log"
	"os"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/urfave/cli"

	"gitlab.com/mradile/rssfeeder/client/config"
	"gitlab.com/mradile/rssfeeder/client/http"
)

const defaultCategory = "default"

var version string

var serverURI string
var category string

var userLogin string
var userPassword string

func main() {
	cliApp := cli.NewApp()
	cliApp.Name = "rssfeeder"
	cliApp.Usage = "rf"
	cliApp.Version = version

	cliApp.Commands = []cli.Command{
		{
			Name:  "add",
			Usage: "add an entry",
			Action: func(c *cli.Context) error {
				url := c.Args().Get(0)
				return add(url)
			},
			Flags: []cli.Flag{
				cli.StringFlag{
					Name:        "category, c",
					Value:       defaultCategory,
					Usage:       "`category` to add an entry to",
					Destination: &category,
				},
				cli.StringFlag{
					Name:        "server, s",
					Value:       "http://localhost:8000",
					Usage:       "`server` uri",
					Destination: &serverURI,
				},
			},
		},

		{
			Name:  "feeds",
			Usage: "list all feeds",
			Action: func(c *cli.Context) error {
				return feeds()
			},
		},

		{
			Name:  "login",
			Usage: "login",
			Action: func(c *cli.Context) error {
				cc := &config.ClientConfig{
					ServerURI: serverURI,
					Login:     userLogin,
					Password:  userPassword,
				}
				return login(cc)
			},
			Flags: []cli.Flag{
				cli.StringFlag{
					Name:        "login, l",
					Value:       "admin",
					Usage:       "`user` for logging in",
					Destination: &userLogin,
				},
				cli.StringFlag{
					Name:        "password, p",
					Value:       "admin",
					Usage:       "`password` for logging in",
					Destination: &userPassword,
				},
				cli.StringFlag{
					Name:        "server, s",
					Value:       "http://localhost:8000",
					Usage:       "`server` uri",
					Destination: &serverURI,
				},
			},
		},
	}

	err := cliApp.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}

func initilalizeRestClient() (*http.RestClient, error) {
	cc, err := config.Load()
	if err != nil {
		return nil, err
	}
	if expired, err := tokenExpired(cc.Token); err != nil {
		return nil, err
	} else if expired {
		fmt.Printf("token expired, trying to login")
		if err := login(cc); err != nil {
			return nil, err
		}
	}

	return http.NewRestClient(cc), nil
}

func feeds() error {
	rc, err := initilalizeRestClient()
	if err != nil {
		return err
	}
	feeds, err := rc.Feeds()
	if err != nil {
		return err
	}
	for _, feed := range feeds.Feeds {
		fmt.Println(feed)
	}
	return nil
}

func add(url string) error {
	rc, err := initilalizeRestClient()
	if err != nil {
		return err
	}

	if err := rc.AddEntry(url, category); err != nil {
		return err
	}

	fmt.Printf("added URL '%s' to '%s' category\n", url, category)
	return nil
}

func login(cc *config.ClientConfig) error {
	rc := http.NewRestClient(cc)

	token, err := rc.Login(userLogin, userPassword)
	if err != nil {
		return err
	}
	if token == "" {
		return fmt.Errorf("could not login, got empty token")
	}
	fmt.Printf("logged in as %s\n", userLogin)
	cc.Token = token

	if err := config.Write(cc); err != nil {
		return err
	}

	return nil
}

func tokenExpired(tokenString string) (bool, error) {
	token, err := jwt.ParseWithClaims(string(tokenString), &jwt.StandardClaims{}, nil)
	if token == nil {
		return false, fmt.Errorf("malformed token: %v", err)
	}
	claims := token.Claims.(*jwt.StandardClaims)
	//fmt.Printf("%d %d ", claims.ExpiresAt, time.Now().Unix())
	return claims.ExpiresAt-time.Now().Unix() < 0, nil
}
