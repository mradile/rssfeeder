FROM alpine:3.8

VOLUME /rssfeederd/data 
VOLUME /rssfeederd/config

WORKDIR /root/

COPY  release/linux-amd64/rssfeederd rssfeederd
COPY  release/linux-amd64/rssfeeder rssfeeder

ENTRYPOINT ["./rssfeederd", "run"]  
