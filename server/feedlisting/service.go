package feedlisting

import (
	"fmt"

	"gitlab.com/mradile/rssfeeder/rssfeeder"
)

const extensions = "[atom,rss,json]"

//Service can list feeds
type Service interface {
	ListFeeds() (*rssfeeder.FeedURLList, error)
}

//Repository can fetch a list of categories
type Repository interface {
	AllCategories() ([]*rssfeeder.Category, error)
}

type service struct {
	r         Repository
	serverURI string
}

// NewService creates a new service instance
func NewService(r Repository, serverURI string) Service {
	return &service{
		r:         r,
		serverURI: serverURI,
	}
}

func (s *service) ListFeeds() (*rssfeeder.FeedURLList, error) {
	cats, err := s.r.AllCategories()
	if err != nil {
		return nil, err
	}

	list := make([]string, 0, len(cats))
	for _, cat := range cats {
		feedURL := fmt.Sprintf("%s/feed/%s/%s.%s", s.serverURI, cat.Name, cat.URLToken, extensions)
		list = append(list, feedURL)
	}

	fl := &rssfeeder.FeedURLList{
		Feeds: list,
	}
	return fl, nil
}
