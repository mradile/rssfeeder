package http

import (
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"strings"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/gorilla/context"
	"golang.org/x/crypto/bcrypt"

	log "github.com/sirupsen/logrus"

	"gitlab.com/mradile/rssfeeder/rssfeeder"
)

//AuthError can be different error ocuring during authentication
type AuthError struct {
	ErrorString string `json:"error_string"`
}

//Error getter for the Error string
func (e *AuthError) Error() string {
	return e.ErrorString
}

//Login can login a user
func (h *Handler) Login(w http.ResponseWriter, r *http.Request) {
	var user rssfeeder.User
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&user); err != nil {
		jsonErrorResponse(w, http.StatusBadRequest, fmt.Errorf("invalid request payload: %s", err))
		return
	}
	defer r.Body.Close()

	if user.Login != h.config.Login || !checkPassword(user.Password, h.config.Password) {
		jsonResponse(w, http.StatusUnauthorized, map[string]string{"message": "wrong login and/or password"})
		return
	}

	token, err := h.getJWTTokenString(user.Login)
	if err != nil {
		//ToDo log error
		jsonErrorResponse(w, http.StatusInternalServerError, fmt.Errorf("could not login user"))
		return
	}

	jsonResponse(w, http.StatusOK, &rssfeeder.JwtToken{Token: token})
	return
}

//ValidateJWTToken validates the JWT provided in the Bearer header
func (h *Handler) ValidateJWTToken(next http.HandlerFunc) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		authHeader := req.Header.Get("Authorization")

		if authHeader == "" {
			authDeniedResponse(w, "Authorization header is empty.")
			return
		}

		//ToDo check if first part is Bearer
		bearerToken := strings.Split(authHeader, " ")
		if len(bearerToken) != 2 {
			authDeniedResponse(w, "Authorization header is malformed")
			return
		}

		jwtToken, err := jwt.ParseWithClaims(bearerToken[1], &RSSFeederJWTClaims{}, func(t *jwt.Token) (interface{}, error) {
			if _, ok := t.Method.(*jwt.SigningMethodHMAC); !ok {
				return nil, fmt.Errorf("There was an error parsing the JWT token")
			}
			return []byte(h.config.Secret), nil
		})
		if err != nil {
			authDeniedResponse(w, "Could not parse JWT token")
			return
		}
		if !jwtToken.Valid {
			authDeniedResponse(w, "JWT token invalid")
			return
		}

		if claims, ok := jwtToken.Claims.(*RSSFeederJWTClaims); ok {
			login := claims.Login
			if login != h.config.Login {
				authDeniedResponse(w, "User does not exist")
				return
			}

			log.WithFields(log.Fields{
				"login": login,
			}).Trace("valid user")

			context.Set(req, "login", login)
			next(w, req)
			return
		}

		jsonResponse(w, http.StatusUnauthorized, AuthError{ErrorString: "Could not authenticate"})
	})
}

func authDeniedResponse(w http.ResponseWriter, message string) {
	jsonResponse(w, http.StatusUnauthorized, AuthError{ErrorString: message})
}

//RSSFeederJWTClaims is custom claim where Login is added as custom field
type RSSFeederJWTClaims struct {
	Login string `json:"login"`
	jwt.StandardClaims
}

func (h *Handler) getJWTTokenString(login string) (string, error) {
	claims := RSSFeederJWTClaims{
		login,
		jwt.StandardClaims{
			ExpiresAt: time.Now().Unix() + 60*60*24, //valid 24h
			Issuer:    h.config.ServerURI,
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	return token.SignedString([]byte(h.config.Secret))
}

func (h *Handler) getLoginFromJWTTokenString(tokenString string) (string, error) {
	jwtToken, err := jwt.ParseWithClaims(tokenString, &RSSFeederJWTClaims{}, func(t *jwt.Token) (interface{}, error) {
		if _, ok := t.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("could not parse JWT token")
		}
		return []byte(h.config.Secret), nil
	})
	if err != nil {
		return "", err
	}

	if claims, ok := jwtToken.Claims.(*RSSFeederJWTClaims); ok {
		return claims.Login, nil
	}
	return "", fmt.Errorf("could not parse JWT token")
}

func hashPassword(password string) (string, error) {
	//speed up unit tests
	cost := 14
	if os.Getenv("RSSFEEDER_TEST") == "1" {
		cost = 4
	}
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), cost)
	return string(bytes), err
}

func checkPassword(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}
