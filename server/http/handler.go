package http

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/gorilla/mux"

	log "github.com/sirupsen/logrus"
	"gitlab.com/mradile/rssfeeder/rssfeeder"
	"gitlab.com/mradile/rssfeeder/server/feeddelivering"
	"gitlab.com/mradile/rssfeeder/server/feeding"
	"gitlab.com/mradile/rssfeeder/server/feedlisting"
)

//Handler defines the REST routes and delegates to the services
type Handler struct {
	feeder        feeding.Service
	feedLister    feedlisting.Service
	feedDeliverer feeddelivering.Service
	router        *mux.Router
	config        *Config
}

//NewHandler create a new handler instance
func NewHandler(config *Config, feeder feeding.Service, feedLister feedlisting.Service, feedDeliverer feeddelivering.Service) *Handler {
	pw, err := hashPassword(config.Password)
	if err != nil {
		log.Fatalf("could not hash password: %v", err)
	}
	config.Password = pw

	r := mux.NewRouter()
	h := &Handler{
		feeder:        feeder,
		feedLister:    feedLister,
		feedDeliverer: feedDeliverer,
		router:        r,
		config:        config,
	}

	r.Use(logRequest)

	r.HandleFunc("/ping", h.Ping).Methods("GET")

	r.HandleFunc("/login", h.Login).Methods("POST")

	r.HandleFunc("/api/v1/feedentry", h.ValidateJWTToken(h.AddFeedEntry)).Methods("PUT")

	r.HandleFunc("/feeds", h.ValidateJWTToken(h.RSSFeeds)).Methods("GET")

	r.HandleFunc("/feed/{category}/{token}.{ext}", h.RSSFeed).Methods("GET")

	return h
}

//RSSFeeds returns a list of RSS feed URIs
func (h *Handler) RSSFeeds(w http.ResponseWriter, r *http.Request) {
	list, err := h.feedLister.ListFeeds()
	if err != nil {
		jsonErrorResponse(w, http.StatusInternalServerError, err)
		return
	}
	jsonResponse(w, http.StatusOK, list)
}

//RSSFeed returns an RSS feed
func (h *Handler) RSSFeed(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	category := vars["category"]
	feed, err := h.feedDeliverer.GetFeed(category)
	if err != nil {
		jsonErrorResponse(w, http.StatusInternalServerError, err)
		return
	}

	w.Header().Set("Content-Type", "application/xml")

	var str string
	var marshalErr error
	switch vars["ext"] {
	case "json":
		str, marshalErr = feed.GetFeedAsJSONString()
		w.Header().Set("Content-Type", "application/json")
	case "rss":
		str, marshalErr = feed.GetFeedAsRSSString()
	case "atom":
		str, marshalErr = feed.GetFeedAsAtomString()
	default:
		str, marshalErr = feed.GetFeedAsAtomString()
	}

	if marshalErr != nil {
		jsonErrorResponse(w, http.StatusInternalServerError, marshalErr)
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write([]byte(str))
}

//AddFeedEntry adds a feed entry
func (h *Handler) AddFeedEntry(w http.ResponseWriter, r *http.Request) {
	var entry rssfeeder.FeedEntry
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&entry); err != nil {
		jsonErrorResponse(w, http.StatusBadRequest, fmt.Errorf("invalid request payload: %s", err))
		return
	}
	defer r.Body.Close()
	if err := h.feeder.AddFeedEntry(&entry); err != nil {
		jsonErrorResponse(w, http.StatusBadRequest, fmt.Errorf("could not save FeedEntry: %s", err))
		return
	}
	w.WriteHeader(http.StatusNoContent)

}

//Ping just answers pong
func (h *Handler) Ping(w http.ResponseWriter, r *http.Request) {
	jsonResponse(w, http.StatusOK, map[string]string{"message": "pong"})
}

//helper functions
func jsonResponse(w http.ResponseWriter, httpCode int, data interface{}) {
	response, _ := json.Marshal(data)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(httpCode)
	w.Write(response)
}

func jsonErrorResponse(w http.ResponseWriter, httpCode int, err error) {
	jsonResponse(w, httpCode, map[string]error{"error": err})
}
