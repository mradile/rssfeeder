package http

import (
	"net/http"
	"time"

	log "github.com/sirupsen/logrus"
)

func logRequest(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		defer func(start time.Time) {
			log.WithFields(log.Fields{
				"duration": time.Since(start),
				"method":   r.Method,
				"url":      r.URL,
			}).Info("Request")
		}(time.Now())
		next.ServeHTTP(w, r)
	})
}
