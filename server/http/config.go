package http

//Config holds information neede for starting the server
type Config struct {
	ServerURI string
	Login     string
	Password  string
	Secret    string
}
