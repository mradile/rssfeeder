package http

import (
	"net/http"
	"strconv"
	"time"

	log "github.com/sirupsen/logrus"
)

//Server starts an http.Server
type Server struct {
	server  *http.Server
	address string
}

//NewServer creates a Server instance which will server under the port given
func NewServer(port int, handler *Handler) *Server {
	address := ":" + strconv.Itoa(port)
	return &Server{
		address: address,
		server: &http.Server{
			Handler:      handler.router,
			Addr:         address,
			WriteTimeout: 30 * time.Second,
			ReadTimeout:  15 * time.Second,
		},
	}
}

//Start starts the server
func (s *Server) Start() error {
	log.WithFields(log.Fields{
		"address": s.address,
	}).Info("Starting service")
	return s.server.ListenAndServe()
}
