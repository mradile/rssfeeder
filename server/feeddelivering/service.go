package feeddelivering

import (
	"fmt"
	"time"

	"github.com/gorilla/feeds"
	"gitlab.com/mradile/rssfeeder/rssfeeder"
)

//RSSFeed represnts an RSS feed
type RSSFeed interface {
	GetFeedAsAtomString() (string, error)
	GetFeedAsRSSString() (string, error)
	GetFeedAsJSONString() (string, error)
}

type rssFeed struct {
	feed *feeds.Feed
}

//GetFeedAsAtomString returns an ATOM feed string
func (rf *rssFeed) GetFeedAsAtomString() (string, error) {
	return rf.feed.ToAtom()
}

//GetFeedAsRssString returns a RSS feed string
func (rf *rssFeed) GetFeedAsRSSString() (string, error) {
	return rf.feed.ToRss()
}

//GetFeedAsRssString returns a JSON string
func (rf *rssFeed) GetFeedAsJSONString() (string, error) {
	return rf.feed.ToJSON()
}

//Service return an RSSFeed instance for the given category
type Service interface {
	GetFeed(category string) (RSSFeed, error)
}

//Repository is an interface to a storage that fetch all feed entries for a given category
type Repository interface {
	AllByCategory(category string) ([]*rssfeeder.FeedEntry, error)
}

type service struct {
	r         Repository
	serverURI string
}

// NewService creates an adding service with the necessary dependencies
func NewService(r Repository, serverURI string) Service {
	return &service{
		r:         r,
		serverURI: serverURI,
	}
}

func (s *service) GetFeed(category string) (RSSFeed, error) {
	entries, err := s.r.AllByCategory(category)
	if err != nil {
		return nil, err
	}

	now := time.Now()
	f := &feeds.Feed{
		Title:       fmt.Sprintf("RSS Feeder - %s", category),
		Link:        &feeds.Link{Href: s.serverURI + "/"},
		Description: category,
		Author:      &feeds.Author{Name: "rssfeeder"},
		Created:     now,
		Updated:     now,
	}

	for _, entry := range entries {
		desc := entry.Description
		if desc == "" {
			desc = entry.URL
		}
		fi := &feeds.Item{
			Title:       entry.URL,
			Link:        &feeds.Link{Href: entry.URL},
			Description: desc,
			Author:      &feeds.Author{Name: "", Email: ""},
			Created:     entry.Time,
			Id:          fmt.Sprintf("%s - %s", entry.Category, entry.URL),
		}
		f.Add(fi)
	}

	rf := &rssFeed{
		feed: f,
	}
	return rf, nil
}
