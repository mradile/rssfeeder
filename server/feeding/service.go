package feeding

import "gitlab.com/mradile/rssfeeder/rssfeeder"

//Service can add feed entries
type Service interface {
	AddFeedEntry(fe *rssfeeder.FeedEntry) error
}

//Repository is an interface to a storage that can add feed entries
type Repository interface {
	AddFeedEntry(fe *rssfeeder.FeedEntry) error
}

type service struct {
	r Repository
}

// NewService creates an adding service with the necessary dependencies
func NewService(r Repository) Service {
	return &service{r}
}

func (s *service) AddFeedEntry(fe *rssfeeder.FeedEntry) error {
	return s.r.AddFeedEntry(fe)
}
