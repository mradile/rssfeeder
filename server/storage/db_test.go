package storage

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/mradile/rssfeeder/rssfeeder"
)

func TestNewBoltDB(t *testing.T) {
	db := testGetBoltDB()
	assert.NotNil(t, db)
	defer db.Close()
}

func testGetBoltDB() Storage {
	os.Remove(getDBFilePath(os.TempDir()))
	return NewStorage(os.TempDir())
}

func TestCategories(t *testing.T) {
	tests := map[string]struct {
		entries []*rssfeeder.FeedEntry
		exp     []string
	}{
		"single": {
			entries: []*rssfeeder.FeedEntry{
				&rssfeeder.FeedEntry{
					URL:      "single",
					Category: "single",
				},
			},
			exp: []string{
				"single",
			},
		},
		"default": {
			entries: []*rssfeeder.FeedEntry{
				&rssfeeder.FeedEntry{
					URL: "single",
				},
			},
			exp: []string{
				defaultCategory,
			},
		},
		"three": {
			entries: []*rssfeeder.FeedEntry{
				&rssfeeder.FeedEntry{
					URL:      "one",
					Category: "one",
				},
				&rssfeeder.FeedEntry{
					URL:      "oneone",
					Category: "one",
				},
				&rssfeeder.FeedEntry{
					URL:      "two",
					Category: "two",
				},
				&rssfeeder.FeedEntry{
					URL:      "one",
					Category: "two",
				},
				&rssfeeder.FeedEntry{
					URL:      "one",
					Category: "three",
				},
			},
			exp: []string{
				"one",
				"two",
				"three",
			},
		},
	}

	for name, test := range tests {
		func() {
			stor := testGetBoltDB()
			defer stor.Close()
			t.Logf("Running test case: %s", name)
			for _, fe := range test.entries {
				assert.Nil(t, stor.AddFeedEntry(fe))
			}

			act, err := stor.AllCategories()
			assert.Nil(t, err)
			assert.NotNil(t, act)
			assert.Equal(t, len(test.exp), len(act))
			for _, cat := range act {
				assert.Contains(t, test.exp, cat.Name)
			}
		}()
	}
}

func TestFeedEntries(t *testing.T) {
	tests := map[string]struct {
		entries []*rssfeeder.FeedEntry
		exp     map[string][]*rssfeeder.FeedEntry
	}{
		"default cat": {
			entries: []*rssfeeder.FeedEntry{
				&rssfeeder.FeedEntry{
					URL:      "11",
					Category: defaultCategory,
				},
			},
			exp: map[string][]*rssfeeder.FeedEntry{
				defaultCategory: []*rssfeeder.FeedEntry{
					&rssfeeder.FeedEntry{
						URL:      "11",
						Category: defaultCategory,
					},
				},
			},
		},
		"two cats": {
			entries: []*rssfeeder.FeedEntry{
				&rssfeeder.FeedEntry{
					URL:      "1",
					Category: "cat1",
				},
				&rssfeeder.FeedEntry{
					URL:      "3",
					Category: "cat1",
				},
				&rssfeeder.FeedEntry{
					URL:      "2",
					Category: "cat2",
				},
			},
			exp: map[string][]*rssfeeder.FeedEntry{
				"cat1": []*rssfeeder.FeedEntry{
					&rssfeeder.FeedEntry{
						URL:      "1",
						Category: "cat1",
					},
					&rssfeeder.FeedEntry{
						URL:      "3",
						Category: "cat1",
					},
				},
				"cat2": []*rssfeeder.FeedEntry{
					&rssfeeder.FeedEntry{
						URL:      "2",
						Category: "cat2",
					},
				},
			},
		},
	}
	for name, test := range tests {
		func() {
			stor := testGetBoltDB()
			defer stor.Close()
			t.Logf("Running test case: %s", name)
			for _, fe := range test.entries {
				assert.Nil(t, stor.AddFeedEntry(fe))
			}

			for catName, exp := range test.exp {
				act, err := stor.AllByCategory(catName)
				assert.Nil(t, err)
				assert.Equal(t, exp, act)
			}
		}()
	}

	stor := testGetBoltDB()
	defer stor.Close()

	//no url
	assert.NotNil(t, stor.AddFeedEntry(&rssfeeder.FeedEntry{Category: "aa"}))

}
