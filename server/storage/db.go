package storage

import (
	"crypto/rand"
	"encoding/json"
	"fmt"
	"path"

	"gitlab.com/mradile/rssfeeder/rssfeeder"

	log "github.com/sirupsen/logrus"
	bolt "go.etcd.io/bbolt"
)

const dbFileName = "/rssfeeder.bolt.db"
const rootBucketName = "ROOT"
const categoryBucketName = "CATEGORY"
const feedEntryBucketName = "FEED_ENTRY"

const defaultCategory = "default"

//Storage has is the port for persistence
type Storage interface {
	AddFeedEntry(fe *rssfeeder.FeedEntry) error
	AllCategories() ([]*rssfeeder.Category, error)
	AllByCategory(category string) ([]*rssfeeder.FeedEntry, error)
	Close() error
}

type boltDB struct {
	db         *bolt.DB
	dbFilePath string
}

type notFoundError struct {
	ErrorString string `json:"message"`
}

func (e *notFoundError) Error() string {
	return e.ErrorString
}

//NewStorage creates a new Storage instance. Path in dbPath must exist.
func NewStorage(dbPath string) Storage {
	dbFilePath := getDBFilePath(dbPath)
	db, err := bolt.Open(dbFilePath, 0666, nil)
	if err != nil {
		log.Fatalf("Could not open bolt db file %s: %v", dbFilePath, err)
	}
	log.WithFields(log.Fields{
		"path": dbFilePath,
	}).Info("Opened database")

	db.Update(func(tx *bolt.Tx) error {
		if _, err := tx.CreateBucketIfNotExists([]byte(rootBucketName)); err != nil {
			return fmt.Errorf("could not create root bucket: %s", err)
		}
		return nil
	})

	return &boltDB{
		db:         db,
		dbFilePath: dbFilePath,
	}
}

//AddFeedEntry ads a FeedEntry
func (boltDB *boltDB) AddFeedEntry(fe *rssfeeder.FeedEntry) error {
	if fe.Category == "" {
		fe.Category = defaultCategory
	}
	if !boltDB.existsCategory(fe.Category) {
		if err := boltDB.addCategory(fe.Category); err != nil {
			return err
		}
	}

	return boltDB.db.Update(func(tx *bolt.Tx) error {
		b, err := getFeedEntryBucketByCategoryName(fe.Category, tx)
		if err != nil {
			return err
		}
		buf, err := json.Marshal(fe)
		if err != nil {
			return err
		}
		return b.Put([]byte(fe.URL), []byte(buf))
	})
}

//AllCategories returns a list of all categories
func (boltDB *boltDB) AllCategories() ([]*rssfeeder.Category, error) {
	var list []*rssfeeder.Category
	err := boltDB.db.View(func(tx *bolt.Tx) error {
		root := tx.Bucket([]byte(rootBucketName))
		c := root.Cursor()
		for k, _ := c.First(); k != nil; k, _ = c.Next() {
			categoryName := k
			feedBucket := root.Bucket([]byte(categoryName))
			categoryBucket := feedBucket.Bucket([]byte(categoryBucketName))

			byteCategory := categoryBucket.Get([]byte(categoryName))
			if byteCategory == nil {
				return &notFoundError{ErrorString: fmt.Sprintf("%s not found in %s", categoryName, categoryBucketName)}
			}

			var cat rssfeeder.Category
			if err := json.Unmarshal(byteCategory, &cat); err != nil {
				log.WithFields(log.Fields{
					"key":   categoryName,
					"value": string(byteCategory),
					"error": err,
				}).Error("could not unmarshal Category")
				continue
			}
			list = append(list, &cat)
		}

		return nil
	})

	if err != nil {
		return nil, err
	}

	return list, nil
}

//AllByCategory returns a list of FeedEntries for given category.
func (boltDB *boltDB) AllByCategory(category string) ([]*rssfeeder.FeedEntry, error) {
	if category == "" {
		category = defaultCategory
	}
	list, err := boltDB.getAllFromBucket(category)
	if err != nil {
		return nil, err
	}
	return list, nil
}

//Close closes db handle
func (boltDB *boltDB) Close() error {
	return boltDB.db.Close()
}

func getDBFilePath(dbPath string) string {
	return path.Join(dbPath, dbFileName)
}

////////////////////
func (boltDB *boltDB) existsCategory(category string) bool {
	exists := false
	boltDB.db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(rootBucketName))
		if b.Bucket([]byte(category)) != nil {
			exists = true
		}

		return nil
	})
	return exists
}

//addCategory initializes a new category
//structure is
//ROOT: {
//	"category": {
//		CATEGORY:   ["category": rssfeeder.Category]
//		FEED_ENTRY: ["url": rssfeeder.Feedentry, "url": rssfeeder.Feedentry]
//	}
//}
func (boltDB *boltDB) addCategory(category string) error {
	tx, err := boltDB.db.Begin(true)
	if err != nil {
		return err
	}
	defer tx.Rollback()

	//create bucket named after category
	root := tx.Bucket([]byte(rootBucketName))
	feedBucket, err := root.CreateBucketIfNotExists([]byte(category))
	if err != nil {
		return err
	}

	//create bucket for holding catgeory struct
	categoryBucket, err := feedBucket.CreateBucketIfNotExists([]byte(categoryBucketName))
	if err != nil {
		return err
	}
	//create bucket holding feed entries of catgeory
	_, err = feedBucket.CreateBucketIfNotExists([]byte(feedEntryBucketName))
	if err != nil {
		return err
	}

	cat := rssfeeder.Category{
		Name:     category,
		URLToken: generateStandardToken(),
	}
	buf, err := json.Marshal(cat)
	if err != nil {
		return err
	}
	if err := categoryBucket.Put([]byte(cat.Name), []byte(buf)); err != nil {
		return err
	}

	if err := tx.Commit(); err != nil {
		return err
	}
	return nil
}

func getFeedEntryBucketByCategoryName(category string, tx *bolt.Tx) (*bolt.Bucket, error) {
	root := tx.Bucket([]byte(rootBucketName))
	if root == nil {
		return nil, &notFoundError{ErrorString: fmt.Sprintf("bucket %s does not exist.", rootBucketName)}
	}
	feedBucket := root.Bucket([]byte(category))
	if feedBucket == nil {
		return nil, &notFoundError{ErrorString: fmt.Sprintf("bucket %s does not exist.", category)}
	}
	feedEntryBucket := feedBucket.Bucket([]byte(feedEntryBucketName))
	if feedEntryBucket == nil {
		return nil, &notFoundError{ErrorString: fmt.Sprintf("bucket %s does not exist.", feedEntryBucketName)}
	}
	return feedEntryBucket, nil
}

func (boltDB *boltDB) getAllFromBucket(category string) ([]*rssfeeder.FeedEntry, error) {
	list := make([]*rssfeeder.FeedEntry, 0)
	err := boltDB.db.View(func(tx *bolt.Tx) error {
		b, err := getFeedEntryBucketByCategoryName(category, tx)
		if err != nil {
			return err
		}

		c := b.Cursor()
		for k, v := c.First(); k != nil; k, v = c.Next() {
			var fe rssfeeder.FeedEntry
			if err := json.Unmarshal(v, &fe); err != nil {
				log.WithFields(log.Fields{
					"key":   k,
					"value": string(v),
					"error": err,
				}).Error("could not unmarshal FeedEntry")
				continue
			}
			list = append(list, &fe)
		}
		return nil
	})
	return list, err
}

func generateStandardToken() string {
	return generateToken(16)
}

func generateToken(length int) string {
	b := make([]byte, length)
	rand.Read(b)
	return fmt.Sprintf("%x", b)
}

/*

func (boltDB *boltDB) getFeedEntry(category, key string, fe *rssfeeder.FeedEntry) error {
	return boltDB.db.View(func(tx *bolt.Tx) error {
		b, err := getFeedEntryBucketByCategoryName(category, tx)
		if err != nil {
			return err
		}

		v := b.Get([]byte(key))
		if v == nil {
			return &notFoundError{ErrorString: fmt.Sprintf("%s not found in %s", key, category)}
		}
		return json.Unmarshal(v, fe)
	})
}
*/
