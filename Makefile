VERSION := `cat VERSION`
PKG := gitlab.com/mradile/rssfeeder
SOURCES ?= $(shell find . -name "*.go" -type f)

all: clean vet lint cover build-linux-amd64 build-linux-arm7 build-darwin

.PHONY: build-linux-amd64
build-linux-amd64:
	CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -i -v -o release/linux-amd64/rssfeederd -ldflags="-X main.version=${VERSION}" cmd/rssfeederd/*.go
	CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -i -v -o release/linux-amd64/rssfeeder -ldflags="-X main.version=${VERSION}" cmd/rssfeeder/*.go

.PHONY: build-linux-arm7
build-linux-arm7:
	CGO_ENABLED=0 GOOS=linux GOARCH=arm GOARM=7 go build -i -v -o release/linux-arm7/rssfeederd -ldflags="-X main.version=${VERSION}" cmd/rssfeederd/*.go
	CGO_ENABLED=0 GOOS=linux GOARCH=arm GOARM=7 go build -i -v -o release/linux-arm7/rssfeeder -ldflags="-X main.version=${VERSION}" cmd/rssfeeder/*.go

.PHONY: build-darwin
build-darwin:
	CGO_ENABLED=0 GOOS=darwin go build -i -v -o release/darwin/rssfeederd -ldflags="-X main.version=${VERSION}" cmd/rssfeederd/*.go
	CGO_ENABLED=0 GOOS=darwin go build -i -v -o release/darwin/rssfeeder -ldflags="-X main.version=${VERSION}" cmd/rssfeeder/*.go

vet:
	go vet cmd/rssfeeder/*.go && go vet cmd/rssfeederd/*.go

lint:
	@for file in ${SOURCES} ;  do \
		golint $$file ; \
	done

.PHONY: test
test:
	go test ./...

.PHONY: cover
cover:
	go test -coverprofile=cover.out ./...
	go tool cover -func=cover.out

.PHONY: cover-html
cover-html: cover
	go tool cover -html=cover.out

.PHONY: clean
clean:
	rm -rf release/*
	rm -f cover.out
