package rssfeeder

//FeedURLList is a list of URIs of all RSS feeds
type FeedURLList struct {
	Feeds []string `json:"feeds"`
}
