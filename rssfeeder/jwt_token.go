package rssfeeder

//JwtToken represents a JWT
type JwtToken struct {
	Token string `json:"token"`
}
