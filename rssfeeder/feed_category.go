package rssfeeder

//Category represents a a feed and its token
type Category struct {
	Name     string `json:"name"`
	URLToken string `json:"url_token"`
}
