package rssfeeder

import (
	"time"
)

//FeedEntry represents a feed item in an RSS feed
type FeedEntry struct {
	Category    string    `json:"category"`
	URL         string    `json:"url"`
	Time        time.Time `json:"time"`
	Title       string    `json:"string"`
	Description string    `json:"description"`
}
