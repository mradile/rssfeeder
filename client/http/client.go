package http

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"time"

	"gitlab.com/mradile/rssfeeder/client/config"
	"gitlab.com/mradile/rssfeeder/rssfeeder"
)

//RestClient can talk to the server instance
type RestClient struct {
	Config *config.ClientConfig
	client *http.Client
}

//NewRestClient create a new RestClient instance
func NewRestClient(config *config.ClientConfig) *RestClient {
	//fmt.Printf("Using server %s\n", config.ServerURI)

	return &RestClient{
		Config: config,
		client: &http.Client{},
	}
}

//Login logs the user with provided login and password in. If successful, a JWT is returned
func (rc *RestClient) Login(login, password string) (string, error) {
	user := &rssfeeder.User{
		Login:    login,
		Password: password,
	}
	userBytes, err := json.Marshal(user)
	if err != nil {
		return "", err
	}
	uri := rc.getRestURI("/login")
	req, err := http.NewRequest("POST", uri, bytes.NewReader(userBytes))
	if err != nil {
		return "", err
	}
	req.Header.Set("Content-Type", "application/json")
	res, err := rc.client.Do(req)
	if err != nil {
		return "", err
	}

	defer res.Body.Close()
	data, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return "", err
	}
	code := res.StatusCode
	if code == http.StatusNoContent {
		return "", fmt.Errorf("could not login %s", string(data))
	}

	var token rssfeeder.JwtToken
	if err := json.Unmarshal(data, &token); err != nil {
		return "", fmt.Errorf("could not unmarshal response '%s': %v", string(data), err)
	}

	return token.Token, nil
}

//AddEntry ads an entry
func (rc *RestClient) AddEntry(url, category string) error {
	fe := &rssfeeder.FeedEntry{
		Category: category,
		URL:      url,
		Time:     time.Now(),
	}
	feBytes, _ := json.Marshal(fe)
	res, err := rc.makeUserRequest("PUT", "/api/v1/feedentry", bytes.NewReader(feBytes))
	if err != nil {
		return err
	}
	defer res.Body.Close()

	code := res.StatusCode
	if code == http.StatusNoContent {
		return nil
	}

	data, _ := ioutil.ReadAll(res.Body)
	return fmt.Errorf("Could not add url: status code: %d, response: %s", code, (string(data)))
}

//Feeds retrieves a list of all RSS feeds
func (rc *RestClient) Feeds() (*rssfeeder.FeedURLList, error) {
	res, err := rc.makeUserRequest("GET", "/feeds", nil)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	data, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}

	code := res.StatusCode
	if code != http.StatusOK {
		return nil, fmt.Errorf("could not fetch feed list: status code: %d, response: %s", code, (string(data)))
	}

	var list rssfeeder.FeedURLList
	if err := json.Unmarshal(data, &list); err != nil {
		return nil, fmt.Errorf("could not unmarshal response '%s': %v", string(data), err)
	}

	return &list, nil
}

func (rc *RestClient) makeUserRequest(method, url string, body io.Reader) (*http.Response, error) {
	uri := rc.getRestURI(url)
	req, err := http.NewRequest(method, uri, body)
	if err != nil {
		return nil, err
	}
	req.Header.Set("Authorization", "Bearer "+rc.Config.Token)
	req.Header.Set("Content-Type", "application/json")

	return rc.client.Do(req)
}

// func (rc *RestClient) responseToError(response *http.Response) error {
// 	data, err := ioutil.ReadAll(response.Body)
// 	if err != nil {
// 		return err
// 	}
// 	defer response.Body.Close()

// 	var em model.ErrorMessage
// 	if err := json.Unmarshal(data, &em); err != nil {
// 		return err
// 	}
// 	return fmt.Errorf(em.Error)
// }

func (rc *RestClient) getRestURI(url string) string {
	return fmt.Sprintf("%s%s", rc.Config.ServerURI, url)
}
