package config

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"os"
	"path"

	homedir "github.com/mitchellh/go-homedir"
)

const configFilename = "rssfeeder.config.json"

//ClientConfig contains all option for talking to the server instance
type ClientConfig struct {
	ServerURI string `json:"server_uri"`
	Login     string `json:"login"`
	Password  string `json:"password"`
	Token     string `json:"token"`
}

//Load loads config from disk
func Load() (*ClientConfig, error) {
	configPath := getDefaultConfigPath()
	filePath := getConfigFilePath(configPath)
	configBytes, err := ioutil.ReadFile(filePath)
	if err != nil {
		return nil, err
	}

	var config ClientConfig
	if err := json.Unmarshal(configBytes, &config); err != nil {
		return nil, err
	}
	return &config, nil
}

//Write writes config to disk
func Write(config *ClientConfig) error {
	configBytes, err := json.Marshal(config)
	if err != nil {
		return err
	}

	configPath := getDefaultConfigPath()
	if _, err := os.Stat(configPath); os.IsNotExist(err) {
		if err := os.Mkdir(configPath, 0755); err != nil {
			return err
		}
	}
	filePath := getConfigFilePath(configPath)

	if err := ioutil.WriteFile(filePath, configBytes, 0644); err != nil {
		return err
	}

	return nil
}

func getConfigFilePath(configPath string) string {
	return path.Join(configPath, configFilename)
}

func getDefaultConfigPath() string {
	home, err := homedir.Dir()
	if err != nil {
		log.Fatalf("could not determine config default path: %v", err)
	}
	return path.Join(home, "/.config")
}
