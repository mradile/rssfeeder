[![pipeline status](https://gitlab.com/mradile/rssfeeder/badges/master/pipeline.svg)](https://gitlab.com/mradile/rssfeeder/commits/master)
[![coverage report](https://gitlab.com/mradile/rssfeeder/badges/master/coverage.svg)](https://gitlab.com/mradile/rssfeeder/commits/master)

# RSSFeeder
RSSFeeder is a server and cli client for creating and serving RSS feeds. The server stores entries in categories which can be accessed via an RSS feed. Data manipulation is secure by a login and RSS feeds have a random token in its URL.

The storage backend is an embedded boltdb database which stores its data in a file.

# Setup
To use this service, you must first start the server and then login. The default credentials for server and client are admin/admin (and should obviously be changed).

To start the server you must provide the run command. The only mandatory paramter is --secret. The secret is used for signing a JWT which is returned after successful login.

```BASH
rssfeederd run --secret someSecret
```

## Options
### General options
General options must be place before the run command
```
--help                help for rssfeeder
--verbose             verbose
--help,               show help
--version,            print the version
```

### Options for the run command
All options can also provided via ENV variables.

```
--port port, -p port  port for service to listen on (default: 8000) [$PORT]
--db_path path        path for to store database in (default: "/Users/mre") [$DB_PATH]
--uri uri             uri where feeds will be accessible (default: "http://localhost:8000") [$SERVER_URI]
--secret secret       secret for generatring jwt tokens [$SECRET]
--login login         login for accessing the service (default: "admin") [$LOGIN]
--password password   password for accessing the service (default: "admin") [$PASSWORD]
```

#Client
The client can login, add feed entries and print a list of all RSS feeds.

## Login
The client must login before it can add entries to the server or retrieve a feed list:

```BASH
rssfeeder login --login admin --password admin --server http://localhost:8000
```

The client retrieves a JWT and stores it in a config file in $HOME/.config/rssfeeder.config.json. The file should only be readable by the current user as the password is currently stored inside. This step has do be done once, the JWT is refreshed on demand.

## Adding entries
For adding entries to the default category simple call add with an URL. 

```BASH
rssfeeder add http://example.org
```

Add an entry to a category:

```BASH
rssfeeder add --category test http://example.org
```

## Feed list
For getting a overview of all feeds

```BASH
rssfeeder feeds
```
