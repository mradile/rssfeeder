module gitlab.com/mradile/rssfeeder

require (
	github.com/boltdb/bolt v1.3.1 // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/golang/mock v1.2.0
	github.com/gorilla/context v1.1.1
	github.com/gorilla/feeds v1.1.0
	github.com/gorilla/mux v1.6.2
	github.com/mitchellh/go-homedir v1.0.0
	github.com/sirupsen/logrus v1.2.0
	github.com/stretchr/testify v1.2.2
	github.com/urfave/cli v1.20.0
	go.etcd.io/bbolt v1.3.0
	golang.org/x/crypto v0.0.0-20180904163835-0709b304e793
	golang.org/x/sys v0.0.0-20180906133057-8cf3aee42992 // indirect
)
